---
Week: 08
Content: CIS
Initials: MESN
# hide:
#  - footer
---

# Uge 08 - *CIS Benchmarks*

## Emner

Ugens emner er:

- CIS Benmarks og Mitre

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Den studerende forstår hvad sikkerheds benchmarks kan bruges til
- Den studerende forstår hvad CIS benchmarks kan bruges til
- Den studerende forstår samehængen mellem CIS benchmarks og CIS Controls
- Den studerende forstår hvad Mitre ATT&CK databasen kan anvendes til.

### Læringsmål der arbejdes med i faget denne uge
  
**Overordnede læringsmål fra studie ordningen:**  
- **Viden:**  
  - Generelle governance principper / sikkerhedsprocedure  
  - Relevante it-trusler  
- **Færdigheder:**  
  - Den studerende kan følge et benchmark til at sikre opsætning af enheder.  
  
## Skema
  
### Tirsdag
  
|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15  | Introduktion til dagen |
| 08:30   | Oplæg: CIS kontroller |
| 08:45   | Gruppe øvelse: CIS kontroller |
| 09:30   | Opsamling på gruppe øvelse  |
| 09:45   | Pause  |
| 10:00   | Oplæg: CIS benchmarks  |
| 10:15   | Gruppe øvelse: CIS Benchmarks  |
| 11:00   | Opsamling på gruppe øvelse   |
| 11:10   | Oplæg: Mitre ATT&CK  |
| 11:30 | Eftermiddags gruppe øvelse: Mitre ATT&CK   |
