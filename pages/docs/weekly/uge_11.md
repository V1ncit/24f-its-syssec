---
Week: 11
Content: Logning
Initials: MESN
# hide:
#  - footer
---

# Uge 11 - *Logning*

## Emner

Ugens emner er:

- Logning

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Den studerende kan redegøre for hvornår der som minimum bør laves en log linje
- Den studerende har grundlæggende forståelse for log management

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
- Generelle governance principper
- Relevante  it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder:**
- implementere systematisk logning og monitering af enheder (Påbegyndt)
- Analyser logs for incidents og følge et revisionsspor (Påbegyndt)
- **Kompetencer:**
- Håndtere udvælgelse af praktiske mekanismer til  at detektere it-sikkerhedsmæssige hændelser.

## Torsdag

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15 | Introduktion til dagen |
| 08:25 | Gruppe øvelse: Hvad er sikkerhed |
| 08:35 | Opsamling på gruppe øvelse |
| 08:40 | Oplæg: Hvornår skal der altid logges? System & sikkerheds mål, samt sikkerhedskrav. |
| 09:00 | Gruppe øvelse: sikkerheds krav i en (Generisk) web shop|
| 09:35 | Opsamling på Gruppe øvelse| 
| 09:45 | Pause|
| 10:00 | Oplæg: Grundlæggende introduktion til log management|
| 10:15 | Gruppe øvelse: Logmanagement |
| 10:50 | Opsamling på gruppe øvelse |
| 11:00 | Oplæg: CIA-T,Gold standards samt log formater |
| 11:30 | Eftermiddags opgave: Opsætning af Wazuh agent |


