---
Week: 16
Content: System mål, Sikkerheds mål, risiko vurdering og trussels modellering
Initials: MESN
# hide:
#  - footer
---

# Uge 16 - *System mål, Sikkerheds mål, risiko vurdering og trussels modellering*

## Emner

Ugens emner er:

- System mål
- Sikkerheds mål
- risiko vurdering
- trussels modellering

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Den studerende kan identificer simple system mål
- Den studerende kan identificer simple sikkerheds mål
- Den studerende kan udarbejde en simple kvalitativ risiko analyse
- Den studerende kan lave en simple trussels modellering

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
  - Generelle governance principper
- **Færdigheder:** ..
  - Relevante sikkerhedsprincipper til systemsikkerhed
- **Kompetencer:** ..
  - Håndter praktiske værktøjer til at identificer trusler (modellering)

## Skema

### Torsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15  | Introduktion til dagen |
| 08:20 | Oplæg: Sikkerhed og udviklingsproces               |
| 08:40 | Oplæg:  Systemmål               |
| 08:50 | Gruppe opgave med system mål          |
| 09:10 | Opsamling på opgave system mål     |
| 09:20 | Oplæg: Sikkerheds mål               |
| 09:40 | Pause               |
| 10:00 | Gruppe opgave  med sikkerheds mål               |
| 10:20 | Opsamling på opgave med sikkerheds mål              |
| 10:30 | Oplæg: Risiko vurdering               |
| 10:45 | gruppe opgave med riskovudering               |
| 11:05 | Opsamling på opgave med risiko vurdering               |
| 11:10 | Oplæg om trusselsmodellering               |
| 11:30 | Afslutning på dagen               |