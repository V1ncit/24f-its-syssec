---
Week: 18
# hide:
#  - footer
---

# Uge 18 - *Malware*

## Emner

Ugens emner er:

- Malware

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- At den studerende forstår anti virus applikations samhæng med overvåg
- At hver studerende har lavet en opsætning med clamav og maldet
- At hver studerende har lavet en skanning med RKhunter

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
  - Relevante IT trusler
- **Færdigheder:**
- **Kompetencer:**
  - Håndtere værktøjer til at identificere og fjerne/afbøde  forskellige typer af endpoint trusler.

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 12:15 | Introduktion til eftermiddagen |
| 12:30 | Walk and talk: Detekteringens 7 lag og netværks skanninger              |
| 12:50 | Opsamling på walk and talk              |
| 13:00 | Oplæg: Hvad er malware              |
| 13:15 | Gruppe øvelse: Malware og detekteringes 7 lag              |
| 13:50 | Pause              |
| 14:05 | Opsamling på gruppe øvelse              |
| 14:15 | Oplæg: Introduktion til statisk malware analyse              |
| 14:30 | statisk malware øvelse              |
| 14:40 | Opsamling på malware øvelse              |
| 14:50 | Øvelser med Linux og malware              |
| 15:30 | Dagen slut              |
