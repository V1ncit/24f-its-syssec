---
Week: 14
Content: Forenicis & audit.
Initials: MESN
# hide:
#  - footer
---

# Uge 14 - *audit & efterforskningsproces*

## Emner

Ugens emner er:

- Efterforsknings proces.
- Linux audit system

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- At hver studerende har en grundlæggende forståelse for efterforsknings processen
- Den studerende kan udføre audit logning med audit daemon
- At hver studerende har prøvet at opsætte en audit regel for en enkelt file.
- At hver studerende har prøvet at opsætte en audit regel for et directory.
- At hver studerende har prøvet at opsætte en audit regel for et system kald.

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
- Den studerende kan redegøre for hvad der skal logges
- Den studerende har viden om væsentlige forensic processer
- Den studerende har viden om relevante it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder:**
- Den studerende kan analysere logs for hændelser og følge et revisionsspor
- **Kompetencer:**
- Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at dektekter specifikke it-sikkerhedsmæssige hændelser.

## Skema

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15 | Introduktion til dagen |
| 08:30 | Opsamling på log øvelser fra sidst |
| 08:50 | Oplæg: Audit og audit logs |
| 09:05 | Individuelle øvelser: AuditD  i Linux |
| 09:45 | Pause                                |
| 10:00 | Individuelle øvelser forsat|
| 10:20 | Opsamling på individuelle øvelser med AuditD|
| 10:30 | Oplæg: efterforsknings proces |
| 10:50 | Gruppe øvelse: Efterforsknings process|
| 11:15 | Opsamling på gruppe øvelse |
| 11:30 | Lektion slut |

## Øvelser

[Opgave 26 - Install Audit daemon](../exercises/26_Install_Auditd.md)  
[Opgave 27 - Audit a file for changes](../exercises/27_audit_file_for_changes.md)  
[Opgave 28 - Audit a directory for changes](../exercises/28_audit_directory.md)  
[Opgave 29 - Audit Operating system for specific request ](../exercises/29_audit_system_calls.md)  
[Opgave 30 - Create a hash value from a file](../exercises/30_create_hash_from_file.md)  