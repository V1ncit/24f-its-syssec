---
Week: 16
Initials: MESN
# hide:
#  - footer
---

# Uge 16 - *Overvågning med SIEM systemer*

## Emner

Ugens emner er:

- 7 lags modellen for detektering
- Kort introduktion til detekterings strategi
- Wazuh regler og dekoder.

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Alle studerende har prøvet at arbejde med 7 lags modellen for detektering
- Alle studerende har implementeret en dekoder i Wazuh
- Alle studerende har implementeret en regel i Wazuh

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:** ..
- **Færdigheder:**
  - Kan implementerer systematisk logning og monitering af enheder
  - Kan analyser logs for hændelser og følge et revision spor
- **Kompetencer:**
  - Kan håndtere enheder på command line-niveau
  - Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at
    forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser.
  - håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15  | Introduktion til dagen |
| 08:25 | Opsamling fra forrige gange               |
| 08:50 | 5 Minutters pause               |
| 08:55 | Detekteringens 7 Abstraktions lag               |
| 09:25 | Gruppe øvelse: Detekteringens 7 Abstraktions lag               |
| 09:55 | Pause               |
| 10:10 | Opsamling på gruppe øvelse               |
| 10:20 | Kort introduktion til detekterings strategi               |
| 10:30 | Kort introduktion Wazuh dekoder og regler               |
| 10:40 | Wazuh Øvelser               |
| 11:30 | Lektion slut|