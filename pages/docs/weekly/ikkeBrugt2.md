---
Week: 22
Initials: MESN
# hide:
#  - footer
---

# Uge 22 - *Forberedelse til eksamen og kryptografi*

## Emner

Ugens emner er:

- Forberedelse til eksamen
- Kryptografi

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Hver studerende ved hvilken læringsmål der bliver eksamineret i.
- Hver studerende ved hvilken læringsmål dennes semester projekt dækker.
- Hver studerende ved hvordan hashing anvendes til sikring af bruger passwords i Ubuntu.
- Hver studerende har en  forståelse for hvor symmetrisk og assymetrisk kryptering virker. 

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
    - Relevante it-trusler
    - Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder:**
    - Udnytte modforanstaltninger til at sikre opsætning af enheder.
- **Kompetencer:**
    - Håndtere relevante krypteringstiltag

## Skema

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15  | Introduktion til dagen |
| 08:30 | Oplæg: Eksamens gennemgang                |
| 08:45 | Gruppe Øvelse: læringsmål & Semester projekt               |
| 09:15 | Blandet gruppe øvelse: Samling af læringsmål fra semester projektet              |
| 09:35 | Opsamling i plenum  på gruppe øvelserne              |
| 09:45 |  Pause   |
| 10:00 | Eksamens Q&A               |
| 10:15: | Oplæg kryptografi               |
| 11:00 |  Arbejde med semester projekt              |
| 11:30 |  Dagen slut              |

## Kommentarer

- ..
