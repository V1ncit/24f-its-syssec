---
Week: 12
Content: logging
Initials: MESN
# hide:
#  - footer
---

# Uge 12 - Logning forsat

## Emner

Ugens emner er:

- Logning
- Logning i Linux

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Den studerende kan redegøre for hvad der skal logges
- Den studerende har kendskab til Ubuntu logging system
- Den studerende kan udføre grundlæggende arbejde med Ubuntu log system.


### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
- Relevante  it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder:** ..
- implementere systematisk logning og monitering af enheder (Påbegyndt)
- Analyser logs for incidents og følge et revisionsspor (Påbegyndt)
- **Kompetencer:** ..
- Håndtere udvælgelse af praktiske mekanismer til at dektekter it-sikkerhedsmæssige hændelser.

## Skema

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15  | Selvstændigt arbejde med Linux Log System |
| 11:30 | Lektion slut |
  
Opgaverne der skal arbejdes med er:
  
[Opgave 14 - RSyslog regler](../exercises/14_Linux_rsyslog_rules.md)  
[Opgave 15 - Log rotation](../exercises/15_Linux_log_rotate.md)  
[Opgave 16 - slå logning fra](../exercises/16_Linux_Disable_logging.md)  
[Opgave 18 - Applikations logs med apache web server](../exercises/18_Linux_application_logs.md)  
[Eftermiddags opgave - Opsætning af web serverer på Proxmox](../exercises/18_1_NGINX_and_Apache2_Web_servers.md)  