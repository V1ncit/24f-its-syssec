---
Week: 18
Initials: MESN
# hide:
#  - footer
---

# Uge 18 - *CVE,CWE og opdateringer*

## Emner

Ugens emner er:

- CVE
- CWE
- Opdatering

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- At hver studerende kan forklar hvad en CVE er.
- At hver studerende kan forklar hvad en CWE er.
- At hver studerende har lavet et detektering af CVE'er med Wazuh. 

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
  - Relevante it-trusler  
- **Kompetencer:**
  - Håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler.

## Skema

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15 | Introduktion til dagen |
| 08:25 | Oplæg: CVE|              |
| 08:40 | Gruppe øvelse: CVE         |
| 09:05 | Opsamling op gruppe øvelse         |
| 09:15 | Oplæg om opdateringer         |
| 09:20 | Oplæg om CWE          |
| 09:25 | Gruppe øvelse: CWE         |
| 09:45 | Pause         |
| 10:00 | Opsamling på gruppe øvelser         |
| 10:10 | Wazuh øvelse 37         |
| 10:30 |  |
| 11:30 | Pause |
