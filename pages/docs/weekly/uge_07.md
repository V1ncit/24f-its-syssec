---
Week: 07
Content: Grundlæggende Linux
Material:
Initials: MESN
# hide:
#  - footer
---

# Uge 07 - *Grundlæggende introduktion til Linux og operativ systemer*

## Emner

Ugens emner er:

- Introduktion til operativ systemer
- Grundlæggende Linux kommandoer

## Mål for ugen

Den studerende har grundlæggende forståelse for operativ systemer,
og kan navigere grundlæggende i Linux CLI.

### Praktiske mål

- Alle studerende har kendskab til de grundlæggende Linux kommandoer.
- Alle studerende kan navigere i Linux file struktur.
- Alle studerende kan oprette og  slette filer i Linux
- Alle studerende kan oprette og slette directories i Linux
- Alle studernede kan fortag en søg på en file eller mappe i Linux
- Alle studerende kan identificer en proces
- Alle studerende kan "dræbe en proces"

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**  
  
**Viden:**  
    - Relevante it-trusler (Oplæg)  
    - Relevante sikkerhedsprincipper til systemsikkerhed  
  
**Kompetencer:**  
    - Den studerende kan håndtere enheder på command line-niveau

## Skema

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15 | Introduktion til dagen |
| 08:30 | Walk and talk: Hvad handlet dagens forberedelse om|
| 08:50 | Oplæg: Introduktion til operativ systemer|
| 09:10 | Individuel opgave: Navigation i Linux file system|
| 09:40 | Opsamling på Navigation i Linux file system|
| 09:45 | Pause|
| 10:00 | Oplæg: Opgaver med Linux file system struktur |
| 10:05 | Opgave Individuel og gruppe: Linux file system struktur|
| 10:35 | Opsamling på Linux file  system struktur |
| 10:45 | Oplæg: Opgaver med søgning i Linux file system og filer|
| 10:50 | Individuel opgaver: søgning i Linux file system og filer |
| 11:20 | Opsamling / Eftermiddags opgave?|
| 11:30 | Undervisning slut| 

