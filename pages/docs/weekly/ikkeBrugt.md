---
Week: 18
Initials: MESN
# hide:
#  - footer
---

# Uge 18 - *Gruppe & projekt arbejde*

## Emner

Ugens emner er:

- Gruppe arbejde
- Projekt arbejde

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- At hver gruppe har taget hul på arbejdet med deres semester projekt.
- At hver gruppe har afstemt projektmålene internt i gruppen.
- At hver gruppe har udarbejdet en klar definetion af semester projektet.

## Skema

### Torsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 12:15  | Introduktion til dagen |
| 12:25  | Gruppe opgaver om gruppe og projekt arbejde |
| 13:45  | Pause |
| 14:00  | Arbejde på semester projekt samt mulighed for vejledning ift. Systemsikkerhed  |
| 15:30 | Fyraften               |
  
## Opgaver  
[Gruppe og projekt opgave](https://ucl-pba-its.gitlab.io/exercises/system/99_Gruppe_og_projekt_opgaver/)

- ..
