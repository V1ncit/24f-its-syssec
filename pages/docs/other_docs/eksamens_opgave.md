---
title: '22S ITS1 INTRO eksamens spørgsmål'
subtitle: 'Eksamens spørgsmål'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2022-04-04
email: 'nisi@ucl.dk'
left-header: \today
right-header: Eksamens spørgsmål
skip-toc: false
semester: 22S
hide:
  - footer
---

# Dokumentets indhold

Dette dokument indeholder praktiske informationer samt spørgsmål til eksamen i faget: _xxx_.

# Eksamens beskrivelse

Eksamen er beskrevet i den institutionelle del af studieordningen _afsnit x.x.x_

Tidsplan for eksamen kan findes på wiseflow.  
Eksamen er med intern/ekstern censur.

*Indsæt beskrivelse af eksamenform her*


# Eksamens datoer

- Forsøg 1 - YYYY-MM-DD
- Forsøg 2 - YYYY-MM-DD
- Forsøg 3 - YYYY-MM-DD
