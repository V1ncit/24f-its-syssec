# Opgave 4 - Linux file struktur

## Information
Hvert Linux-directory i roden har per konvention et specifikt formål, altså hvilken data de typisk indeholder. For eksempel bør directoryet `var/log` indeholde alle applikationslogs fra applikationer, der bliver eksekveret.

Disse konventioner skal der arbejdes med i denne opgave, først individuelt og siden i teamet.
  
Filstrukturen i Linux ser ud som følger:  
```
/
|_bin
|_boot
|_dev
|_etc
|_home
|_media
|_lib
|_mnt
|_misc
|_proc
|_opt
|_sbin
|_root
|_tmp
|_usr
|_var
```
  
## Instruktioner
1. **Individuelt**: Undersøg og noter i Linux cheatsheet, hvad det er for nogle filer, som er tiltænkt hver enkelt directory.
   _Tids estimering: 20 minutter_
2. **I teamet**: Teamet skal nu sammenligne resultaterne og lave en fælles konklusion af, hvad hvert enkelt directory er tiltænkt at indeholde.
   _Tids estimering: 10 minutter_
## Links
[Basic Linux commands](https://www.hostinger.com/tutorials/linux-commands)  
[Basic linux commands videos](https://www.youtube.com/watch?v=gd7BXuUQ91w)

