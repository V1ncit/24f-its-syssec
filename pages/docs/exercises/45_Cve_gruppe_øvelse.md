# Opgave 45 - CVE gruppe øvelse

## Information
Hensigten med øvelsen er at gruppen sammen får har en dialog om CVE'er, og 
sammen udforske CVE'er 


## Instruktioner
I jeres gruppe skal i sammen undersøge betydningen af følgende kendte
sårbarheder:  
  
- CVE-2023-32269  
- CVE-2023-31436  
- CVE-2014-0160  
- CVE-2022-47509  
- CVE-2021-44228  
- CVE-2022-26903  
  
I skal finde den konceptuelle forklaring på sårbarheden CVE'en skaber, samt
hvor alvorlig sårbarheden er.
  
_Tid: 25 minutter_

## Links
[Mitre cve database](https://cve.mitre.org/cve/search_cve_list.html)  
[Nist sårbarheds database](https://nvd.nist.gov/vuln)