# Opgave 10 - Bruger rettigheder i Linux.

## Information
I tidligere opgave blev konceptet _bruger rettigheder_ kort introduceret. Det skal der arbejdes mere i dybden  med 
i denne opgave.  
  
Bruger rettigheder i linux deles op i 3 segmenter. Ejeren af en files rettigheder. gruppen
som er tilknyttet filens rettigheder, og til sidst alle andres  rettigheder med filen.  
Som udgangspunkt er der 3 rettigheder som kan tildeles en file, det er:  
- Read - Brugeren kan læse filen (og kopier den).  
- Write - Brugeren kan skrive og ændre i filen.    
- Execute - Brugeren kan eksekveren filen som et program.   

Såfremt man ønsker at se rettigheder på en file kan dette gøres med kommandoen `ls -l`
![file rettigheder](./Images/fileRights.jpg)  
I det overstående eksempel har ejeren af filen rettighed til at læse,skrive og eksekver filen. Ingen andre end ejeren har rettigheder til denne file.  
  
Ligeledes kan rettighederne for et directory ses med kommandoen `ls -ld <Directory navn>`  
![Directory rettigheder](./Images/directoryRights.jpg)  
bostavet d som står forrest indikerer at  rettigheder gælder for et directory.  
Ejeren  har læse,skrive og eksekverings rettigheder.   Eksekverings rettigheden på et directory betyder at
selv om man ikke har læse rettigheder, kan man stadig tilgå indholdet (Men ikke liste det med F.eks. `ls` kommandoen).  
  
kommandoen chmod som anvendes til at tildele rettigheder til en file. 
Man kan sætte rettighederne med enten tal eller bogstaver.
Hvis man bruger tal er rækkefølgen for rettigheds tildeling `|Ejer|Gruppen|Alle andre|`.  
Nedstående tabel viser hvilken værdi der bruges til rettigheds kombinationer.  
  
|værdi|rettighed|sum  |
|-----|---------|-----|
|0    |---      |0+0+0|
|1    |--x      |0+0+1|
|2    |-w-      |0+2+0|
|3    |-wx      |0+2+1|
|4    |r--      |4+0+0|
|5    |r-x      |4+0+1|
|6    |rw-      |4+2+0|
|7    |rwx      |4+2+1|  
  
Hvis F.eks. vil give brugeren alle rettigheder, og ingen rettigheder til andre
vil kombinationen være 700. Vil man give ejeren læse og skrive rettigheder, og gruppen
læse rettigheder, og eksekverings rettigheder til alle andre er kombinationen 641.
  
Lidt mere intuitivt kan man bruge bogstaver.
Her vælger man hvilket segment der skal tildeles en rettighed med et bogstav.
  
|User |group    |Other|
|-----|---------|-----|
|u    |g        |o    |  
  
Herefter bruger man +/- operatoren til at tildele rettigheder
jvf. en dertil bestemt karakter  

|Read |Write    |Execute|
|-----|---------|-------|
|r    |w        |x      |  
  
Eksempelvis vil kommandoen `chmod go-rwx` fratage gruppen og alle andre, alle deres rettigheder rettigheder.
  
**Husk at forsætte med at noter i dit Linux cheat sheet**

## Links til beskrivelse af kommandoerne i opgaven:  
- [chmod](https://linux.die.net/man/1/chmod )
- [chown](https://linux.die.net/man/1/chown )


## Instruktioner
I de følgende opgaver skal der arbejdes med tildele og fratagelse af rettigheder til filer og directories.  
**Udfør alle opgaver ved at tildele med både tal og bogstaver**


### Giv og fjern læse rettigheder til en file
I denne opgave skal oprettes en file, og gives læse rettigheder til alle andre.
Fjern derefter rettigheden igen.

### Giv og fjern skrive rettigheder til en file
I denne opgave skal oprettes en file, og gives skrive rettigheder til gruppen.
Fjern derefter rettigheden igen.

### Giv og fjern læse rettigheder til et directory
I denne opgave skal oprettes en directory, og gives læse rettigheder til alle andre.
Fjern derefter rettigheden igen.

### Giv og fjern skrive rettigheder til et directory
I denne opgave skal oprettes en directory, og gives skrive rettigheder til gruppen.
Fjern derefter rettigheden igen.

### Giv og fjern eksekverings rettigheder til en file
I denne opgave skal oprettes en file, og gives eksekverings rettigheder til gruppen.
Fjern derefter rettigheden igen.

### Ændre ejeren af en file.
I denne opgave skal ejerskabet over en file ændres til en anden bruger, med kommandoen `chown`.

### Sårbarhed - For mange rettigheder
Reflekter over konsekvens ved for mange rettigheder. Hvorfor sker det nogen gange?
Og hvorfor søger nogen ikke ondsindet bruger at få flere rettigheder?


## Links

  
