# Opgave 18.1 - Eftermiddagsopgave med webserver på Proxmox-serveren.

## Information
Til semesterprojektet forventes det, at der eksponeres to webservere mod DMZ. I denne eftermiddagsopgave skal I lave en Ubuntu-serveropsætning med Apache-webserver og en med NGINX-webserver. Til enten Apache-webserveren eller NGINX kan I bruge den Ubuntu-serverinstans, I tidligere har sat op, med navnet _Targethost_. Derudover skal I lave endnu en Ubuntu-server-VM, der kan hoste den webserver, som ikke blev sat op. Altså skal der være:
- To Ubuntu-serverinstanser (Hvoraf den ene allerede er sat op tidligere)
- På én instans skal der være en opsætning med Apache2-webserver
- På den anden instans skal der være en opsætning med NGINX-webserver
- Begge instanser skal overvåges af en Wazuh-agent
- Begge instanser skal have individuelle brugerkonti til alle gruppemedlemmer.

## Instruktioner
1. Lav opsætning med Apache2-webserver på en Ubuntu-server-VM-instans. [Du kan følge guiden her](https://ubuntu.com/tutorials/install-and-configure-apache#5-activating-virtualhost-file)
2. Lav opsætning med NGINX-webserver på en anden Ubuntu-server-VM-instans. [Du kan følge guiden her](https://ubuntu.com/tutorials/install-and-configure-nginx#3-creating-our-own-website)
3. Lav individuelle brugerkonti til hvert gruppemedlem.
4. Lav opsætning, så begge Ubuntu-VM-instanser er overvåget.

Når opsætningen er færdig, skal I selvstændigt finde de logfiler, som bliver anvendt af NGINX. Der er en adgangslog og en fejllog.  
  
Lav en tabel hvor i kan se hvilken VM instanser i har nu: F.eks. Opensense, Wazuh Server, Ubuntu server med Apache2,
Ubuntu server med NGINX. I kan se et eksempel på dette i [Eftermiddags opgaven fra uge 9](./Semester_projekt_UbuntuServerer.md ). Sikre jer at alle jeres Promox VM'er er noteret der, samt hvilken rolle de har
(Router,webserver,SIEM osv. osv.)

## Links
[Apache logging basics](https://www.loggly.com/ultimate-guide/apache-logging-basics/)
[Linux log files](https://help.ubuntu.com/community/LinuxLogFiles)