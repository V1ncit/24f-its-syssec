# Opgave 26 - Installer auditd

## Information
Audit daemon (auditd) er det mest anvendte Linux' audit system. Auditd kan overvåge forskellige tilladelser, såsom læs, skriv og udfør.

Typisk anvendes audit systemet til overvågning af filer, mapper og systemkald (kald til OS API'et).

Audit daemon fungerer principielt som et logningsystem for foruddefinerede begivenheder. Alle begivenheder defineres som 'Audit regler'. Et eksempel på en foruddefineret begivenhed kunne være ændringer i en bestemt fil. Hver gang der bliver ændret i filen, udløser dette en begivenhed, som resulterer i en 'Audit log'. En slags "alarm", der udløses af en specifik begivenhed. Som udgangspunkt kan alle auditd logs findes i /var/log/audit/audit.log.

Generelt er auditd ikke særlig intuitivt at arbejde med, men det skaber en god forståelse for, hvordan et sådant system virker.

**Vigtigt! Auditd er ikke et versionsstyringssystem og holder derfor ikke styr på, hvad der blev ændret.**
Audit-systemet kan dog overvåge, hvilken bruger der har ændret hvilke filer, mapper eller foretaget systemkald, men ikke hvad der blev ændret.

## Instruktioner
1. Som standard er audit daemon ikke installeret på Ubuntu. Installer audit daemon ved at køre kommandoen `apt install auditd`.
2. Verificér at auditd er aktivt ved at køre kommandoen `systemctl status auditd`.
![Auditd aktiv](./Images/AuditDActive.jpg)
3. Brug værktøjet `auditctl` til at udskrive nuværende regler med kommandoen `auditctl -l`.
![Ingen regler auditd](./Images/NoRulesAuditd.jpg)
4. Udskriv logfilen, som findes i `/var/log/audit/audit.log`.
![Auditd log](./Images/AuditdLog.jpg)

Hvis loggen virker uoverskuelig, er du ikke alene. Mange finder den svær at læse. Derfor bruger man typisk to værktøjer kaldet `ausearch` og `aureport` til at analysere konkrete begivenheder. Begge værktøjer vil blive gennemgået i de kommende opgaver.

At arbejde direkte med auditd er en smule primitivt sammenlignet med mange andre værktøjer. Men det giver en grundlæggende forståelse for, hvordan man arbejder med audit på et næsten lavt niveau i Linux. Mange større og mere intuitive auditsystemer (f.eks. SIEM-systemer) benytter også auditd eller kan læse auditd-loggen.

## Links
[auditd man-side](https://linux.die.net/man/8/auditd )
  
