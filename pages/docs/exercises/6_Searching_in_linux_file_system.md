# Opgave 6 - Søgning i Linux file strukturen

## Information
Formålet med denne opgave er at introducere de grundlæggende Linux-kommandoer.

I opgaven skal der eksperimenteres med Linux CLI-kommandoer i BASH shell.
Fremgangsmåden er, at du bliver bedt om at eksekvere en kommando, og herefter notere hvad der sker. Målet med disse øvelser er, at du skal opbygge et _Cheat sheet_ med Linux-kommandoer i dit GitLab repo og få en generel rutinering med grundlæggende Linux Bash-kommandoer. Det betyder følgende for alle trin i opgaven:

1. Udfør kommandoen.
2. Observer resultatet, og noter det herefter i dit _Cheat sheet_. _Altså efter hver eksekveret kommando, skal du kunne redegøre for, hvad den gjorde._

## Links til beskrivelse af kommandoerne i opgaven:
[find](https://man7.org/linux/man-pages/man1/find.1.html)

## Instruktioner
**Husk at notere i dit cheatsheet efter hvert trin!**
**En del af opgaven er at træne informations søgning ift. Linux, så lad vær med at bruge ChatGPT**

1. I `Home directory`, eksekver kommandoen `find`.
2. I `Home directory`, eksekver kommandoen `find /etc/`.
3. Eksekver kommandoen `sudo find /etc/ -name passwd`. _Sudo foran kommandoen betyder, at kommandoen eksekveres med de rettigheder, som sudo-gruppen har._
4. Eksekver kommandoen `sudo find /etc/ -name pasSwd`. **Husk stort S**.
5. Eksekver kommandoen `sudo find /etc/ -iname pasSwd`. **Husk stort S**.
6. Eksekver kommandoen `sudo find /etc/ -name pass*`.

_De næste kommandoer bruges til at finde filer baseret på deres byte-størrelse. De to første trin bruges blot til at generere de to filer, som skal findes._

1. I `Home directory`, eksekver kommandoen `truncate -s 6M filelargerthanfivemegabyte`.
2. I `Home directory`, eksekver kommandoen `truncate -s 4M filelessthanfivemegabyte`.
3. I roden (/), eksekver kommandoen `find /home -size +5M`.
4. I roden (/), eksekver kommandoen `find /home -size -5M`.
5. I `Home directory`, opret to directories, et der hedder `test` og et andet som hedder `test2`.
6. I `test2`, skal der oprettes en fil som hedder `test`.
7. I `Home directory`, eksekver kommandoen `find -type f -name test`.
8. I `Home directory`, eksekver kommandoen `find -type d -name test`.

## Links
[Basic Linux commands](https://www.hostinger.com/tutorials/linux-commands)
[Basic Linux commands videos](https://www.youtube.com/watch?v=gd7BXuUQ91w)