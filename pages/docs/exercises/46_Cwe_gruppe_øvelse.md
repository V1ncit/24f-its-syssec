# Opgave 46 - CWE gruppe øvelse

## Information
Hensigten med øvelsen er at gruppen sammen får har en dialog om CWE'er, og 
sammen udforske CWE'er 


## Instruktioner
I jeres gruppe skal i sammen undersøge betydningen af følgende kendte
svagheder:
  
- CWE-287
- CWE-272
- CWE-1329
  

I skal finde den konceptuelle forklaring på sårbarheden CWE'en. Og kunne forklar dem
overordnet.
  
_Tid: 20 minutter_

## Links
[Mitre CWE database](https://cwe.mitre.org/)  
