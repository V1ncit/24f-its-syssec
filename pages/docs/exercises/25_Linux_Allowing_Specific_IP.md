# Opgave 25 - Tillad indgående trafik fra en specifik IP-adresse.

## Information
For at anvende _allow list_-konceptet kan man vælge at give specifikke IP-adresser lov til at kommunikere med værten på en specifik port. I denne opgave skal der åbnes op for kommunikation med en specifik IP-adresse.

## Instruktioner
1. Lav en regel, der tillader modtagelse af beskeder fra en IP-adresse med kommandoen `sudo iptables -A INPUT -p tcp -s <Source IP> --dport 22 -m conntrack --ctstate NEW -j ACCEPT`.
2. Lav en regel, der tillader afsendelse af beskeder til en specifik IP-adresse med kommandoen `sudo iptables -A OUTPUT -p tcp --dport 53 -m conntrack --ctstate NEW -j ACCEPT`.
3. Prøv evt. at ping hosten med firewallen fra en anden VM.

## Links