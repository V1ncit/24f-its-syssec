# Opgave 43 - skan med rkhunter

## Information
Modsat Maldet, så eksekver Rkhunter ikke automatisk sine skanninger. Her skal man manuelt
eksekver skanningerne, eller  lave en opsætning med et CRON job.
I denne opgave skal der eksekveres en skanning med Rkhunter.

*** Jeg har ikke kunne finde nogen simuleret rootkits, kun den  ægtevare, og derfor er der ikke nogen test filer til øvelsen, og skanning bør ikke  resulter i et fund *** 

## Instruktioner
1. eksekver scanning med rkhunter med kommandoen `rkhunter -c` (Du vil blive bedt om at trykke enter engang imellem)
2. Studer log filen `/var/log/rkhunter.log` _Der bør ikke været fundet noget, hvert fald ikke som en del af øvelsen!_
3. Reflekter over hvordan du ville kunne overvåge denne log med f.eks. Wazuh (Brug opgave 38 som inspiration).


## Links
[Rootkit hunter docs](https://rkhunter.sourceforge.net/)