# Opgave 5 - Hjælp i Linux

## Information
Formålet med denne  opgave er at introducerer de grundlæggende linux kommandoer.

I opgaven skal der eksperimenteres med Linux CLI kommandoer i BASH shell.
Fremgangsmåden er at du bliver bedt om at eksekverer en kommando, og herefter noter
hvad der sker. **Målet med disse øvelser** er at du skal bygge et _Cheat sheet_ med linux kommandoer i dit gitlab repositorier, og få en genral rutinering med grundlæggende Linux Bash kommandoer. **Det betyder følgende for alle trin i opgaven:**

1. Udfør kommandoen.
2. Observer resultatet, og noter det herefter i dit _Cheat sheet_
**_Altså efter hver eksekveret kommando, skal du kunne redegøre for hvad den gjorde_**

## Links til beskrivelse af kommandoerne i opgaven:  
[man](https://man7.org/linux/man-pages/man1/man.1.html)

## Instruktioner
1. Eksekver kommandoen `apt --help`.
2. via `apt --help` skal du nu finde ud af hvordan du udskriver en liste til konsolen, som viser alle installeret pakker.
3. Eksekver kommandoen `apt -h`.
4. Eksekver kommandoen `man apt` og scroll ned i bunden af konsol outputtet(pgdwn)
5. Eksekver kommandoen `man ls` og scroll ned i bunden af konsol outputtet
6. Eksekver kommandoen `man man` og skim outputtet, hvilken information kan du finde der?
7. eksekver kommandoen `help`

## Test dig selv  
_Udfør den følgende opgave, med så lidt hjælp som muligt_
1. Udskriv en liste over filerne i `/etc`. Listen skal også indeholde filens index nummber.  _Brug kun hvad der er til råddighed i Linux, Ingen Google_

## Links
[Basic Linux commands](https://www.hostinger.com/tutorials/linux-commands)  
[Basic linux commands videos](https://www.youtube.com/watch?v=gd7BXuUQ91w)

