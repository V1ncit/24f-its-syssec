# Opgave 48 - (Gruppeopgave) Mitre ATT&CK Taktikker, Teknikker, mitigering & detektering

## Information
Formålet med denne øvelse er at udforske Mitre ATT&CK-rammeværket med fokus på taktikken Privilege Escalation (Forhøjelse af Privilegier), teknikken T1548, samt afhjælpningen M1026 & Detekteringen DS0022.
  
Opgaven er en eftermiddagsopgave, og det forventes, at gruppen laver undersøgelsen sammen.  
  
**I bliver kastet ud i den dybe ende med denne opgave. Det er meningen med øvelsen (Det og naturligvis at I skal lære Mitre at kende)**

## Instruktioner
1. Undersøg Mitre ATT&CK taktikken TA0004. Hvad betyder denne taktik?
2. Identificer specifikke under-teknikker under T1548 og hvordan de bidrager til at opnå Privilege Escalation.
3. Identificer og undersøg Mitigeringen M1026.
4. Identificer og undersøg detekteringen DS0022.
5. Til næste uge, forbered en kort præsentation, der opsummerer dine resultater, herunder en forklaring på Privilege Escalation-taktikken, teknikkerne under T1548, mitigeringen M1026 samt detekteringen DS0022.  
  
   _Det behøver ikke at være en stor og forkromet præsentation, blot noget i stil med "Daniel, gæstelæreren", når han præsenterer resultatet af hans øvelser. Altså maks 5 minutter_

## Spørgsmål til diskussion i gruppen
1. Hvad er formålet med Privilege Escalation-taktikken (TA0004) inden for Mitre ATT&CK-rammeværket, og hvorfor er det en vigtig del af en angribers taktikker?
2. Hvordan bidrager teknikkerne under T1548 til angriberens evne til at opnå Privilege Escalation?
3. Hvad er karakteristika for mitigeringen M1026, og hvordan hjælper den med at afhjælpe risiciene forbundet med Privilege Escalation?
