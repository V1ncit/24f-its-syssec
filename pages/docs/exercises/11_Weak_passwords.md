# Opgave 11 - Brugerkontoer med svage passwords
  
## Information
Selv når der anvendes en stærk hash algoritme. Så yder hashet kun begrænset
beskyttelse til svage kodeord. Dette skal der eksperimenteres med i denne opgave.
  
I opgaven skal der oprettes en bruger med et svagt kodeord. Herefter skal du 
trække kodeord hashet ud fra shadow filen, og forsøg genskabe passwordet ud
fra hash værdien. 

## Instruktioner
  
**Alle kommandoer skal eksekveres mens du er i dit hjemme directory**  
1. installer værktøjer `john-the-ripper` med kommandoen `sudo apt install john`  
2. Her efter skal du downloade en _wordlist_ kaldet rockyou med følgende kommando `wget https://github.com/brannondorsey/naive-hashcat/releases/download/data/rockyou.txt`.  
3. Opret nu en bruger ved navn `misternaiv` og giv ham passwordet `password`  
4. Hent nu det hashet kodeord for misternaiv med følgende kommando `sudo cat /etc/shadow | grep misternaiv > passwordhash.txt`  
5. udskriv indholdet af filen `passwordhash.txt`, og bemærk hvilken krypterings algoritme der er brugt.  
6. Eksekver nu kommandoen `john -wordlist=rockyou.txt passwordhash.txt`.  
7. Kommandoen resulter i `No password loaded`. Dette er fordi john the ripper værktøjet ikke selv har kunne detekter hvilken algoritme det drejer sig om.  
8. Eksekver nu kommandoen:   `john --format=crypt -wordlist=rockyou.txt passwordhash.txt`.  
   _format fortæller john the ripper hvilken type algoritme det drejer sig om_  
9.  Resultat skulle gerne være at du nu kan se kodeordet, som vist på billede nedenunder.  
![password crack](./Images/passwordCracket.jpg)  
  
10.  gentag processen fra trin 1 af. Men med et stærkt password.(minnimum 16 karakterer, både store og små bogstaver, samt special tegn)  
11.  Reflekter over hvorfor komplekse kodeord er vigtige (Og andre gange en hæmmesko for sikkerheden).  
    

  
