# Opgave 24 - Tilad SSH trafik

## Information
For at kunne opnå fjern adgang til vores host, er det nødvendigt at tillade SSH trafik.
 
## Instruktioner
1. Tillad SSH forbindelse med kommandoen `sudo iptables -A INPUT -p tcp --dport ssh -j ACCEPT`
2. Istedet for at skrive SSH er der også en specifik port som der typisk vil kunne åbnes op for i stedet, hvilken en port er det?

## Links

  
