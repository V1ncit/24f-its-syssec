# Opgave 48 - (Gruppeopgave) Ubuntu CIS Benchmarks

## Information

Formålet med denne øvelse er at gruppen skal foretage en vurdering af et Ubuntu-systems overholdelse af udvalgte CIS Ubuntu benchmarks sikkerhedsanbefalinger.

**Bemærk:** Arbejdet, I skal udføre nu, svarer til at udføre en revision (audit) i forhold til CIS 18 compliance. Man bruger typisk automatiske værktøjer til dette, men værktøjer bruger ofte kommandoer. CIS-benchmarken er ret omfattende, så vælg benchmarks ud fra jeres niveau.

## Instruktioner

1. Download CIS Ubuntu Linux-benchmark-dokumentet fra It's learning (under ressourcer til dagens lektion).
2. Skim afsnittet "Overview", og læg mærke til, at alle handlinger skal udføres som root-brugeren (og ikke med sudo-bruger).
3. Udvælg en eller flere benchmarks at arbejde med.
4. Brug en af gruppens medlemmers Ubuntu-serverinstans til at gennemgå de udvalgte benchmarks fra CIS-benchmarken.
5. For hvert punkt i benchmarken skal I undersøge, om systemet overholder anbefalingerne (under punktet "audit").
6. Identificer eventuelle åbenlyse afvigelser fra anbefalingerne.
7. Identificer eventuelle foranstaltninger og implementer dem.