# Opgave 37 - Wazuh skanning for sårbar software.

## Information
Alle Wazuh opgaverne indtil videre, stammer fra Wazuh dokumentationen _Wazuh proof of concept_.  
En af de allervigtigste kompetencer man kan få inden for IT, er evnen til at lære nye technologier, koncepter osv.  
Derfor skal der i denne opgave implementeres sårbarheds skanning, men det skal gøres jvf. Wazuh's egen dokumentation.

## Instruktioner
1. Følg [guiden](https://documentation.wazuh.com/current/proof-of-concept-guide/poc-vulnerability-detection.html) til opsætning af sårbarheds skanning.
_Den første fuld skanning af hosten kan godt tage lidt tid_

## Links
[How vulnerability scannings works in wazuh](https://documentation.wazuh.com/current/user-manual/capabilities/vulnerability-detection/index.html)
