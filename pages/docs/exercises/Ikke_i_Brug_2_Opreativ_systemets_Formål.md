# Opgave 2 - Operativ systemets formål

## Information
I denne lektion startes der op med vidensdeling ud fra den forberedelse i har læst.
Formålet med anvendelse af vidensdeling i starten af hver lektion, er at sikre at alle maksimerer sit
udbytte af forberedelserne, ved at få så mange perspektiver som muligt ift. det emne som forberedelsen formidlet.
CL strukturen anvendes meget i starten, for at give teamet en struktur at arbejde udfra, og sikre at alle team medlemmer
kommer til ord i begyndelsen af samarbejdet. 

## Instruktioner

1. Hvert team medlem skal med udgangspunkt i forberedelsen til idag, reflektere over formålet med operativ systemer og deres kontekst ift. IT-sikkerhed.  
_Tids estimate: 10 minutter_
1. Hvert team medlem skal noter så mange som muligt af sine overvejelser på et stykke papir eller i en tekst editor.  
_Tids estimate: 10 minutter_  
1. Teamet skal nu benytte CL strukturen [Møde på midten](../Cooperative%20Learning%20Strukturer/Møde%20på%20midten.docx) til
   at udarbejde en fælles formulering.
_Tids estimate: 15 minutter_
1. Teamet skal skrive den fælles formulering ind i padlet.   
_Tids estimate: 5 minutter_

## Links
[Basic Linux commands](https://www.hostinger.com/tutorials/linux-commands)  
[Basic linux commands videos](https://www.youtube.com/watch?v=gd7BXuUQ91w)
 
