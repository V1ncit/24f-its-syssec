# Opgave 21 - Tillad indgående trafik fra etableret forbindelser

## Information
iptables er en "stateful" firewall. Det betyder at den blandt andet kan holde styr på hvilken
forbindelser operativ systemet har oprettet til eksterne server. Dette skal vi arbejde med i den
følgende opgave. 
 
## Instruktioner
1. Eksekver  kommandoen `sudo iptables -I INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT`
2. Udskriv reglen listen.

I kommandoen vælger vi at indsætte reglen først i regelen kæden med `-I` muligheden. Herefter vælges
det er et iptables module med `-m conntrack` (conntrack==connection track) for at iptables kan holde
styr på hvilken forbindelser der er etableret, og af hvem. Til sidst bruges muligheden `--ctstate ESTABLISHED,RELATED -j ACCEPT`
som tillader indadgående pakker fra serverer hvor klienten(OS) selv har oprettet forbindelse. 

## Links

  
