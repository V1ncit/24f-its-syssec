# Opgave 3 - Navigation i Linux filesystem med bash

## Information
Formålet med denne opgave er at introducere de grundlæggende Linux-kommandoer.

I opgaven skal der eksperimenteres med Linux CLI-kommandoer i BASH shell. Fremgangsmåden er, at du bliver bedt om at eksekvere en kommando, og herefter notere hvad der sker. Målet med disse øvelser er, at du skal opbygge et "Cheat sheet" med Linux-kommandoer i dit GitLab-repositorium og få en generel rutinering med grundlæggende Linux Bash-kommandoer. Det betyder følgende for alle trin i opgaven:

1. Udfør kommandoen.
2. Observer resultatet, og noter det herefter i dit "Cheat sheet".
  
Altså, efter hver eksekveret kommando skal du kunne redegøre for, hvad den gjorde.

## Links til beskrivelse af kommandoerne i opgaven:  
[pwd](https://man7.org/linux/man-pages/man1/pwd.1.html)  
[cd](https://man7.org/linux/man-pages/man1/cd.1p.html)  
[ls](https://man7.org/linux/man-pages/man1/ls.1.html)  
[touch](https://man7.org/linux/man-pages/man1/touch.1.html)  
[mkdir](https://man7.org/linux/man-pages/man1/mkdir.1.html)  

## Instruktioner
**Husk at notere i dit cheatsheet efter hvert trin!**
**En del af opgaven er at træne informations søgning ift. Linux, så lad vær med at bruge chatgpt**

1. Eksekver kommandoen `pwd`.
2. Eksekver kommandoen `cd ..`.
3. Eksekver kommandoen `cd /`.
4. Lokationen `/` har en bestemt betydning i Linux-filsystemet. Hvad er det? (Her kan du med fordel søge svar på Google).
5. Eksekver kommandoen `cd /etc/ca-certificates/`. Hvad findes der i directoriet?
6. Hvor mange _directories_ viser outputtet fra `pwd` kommandoen når den eksekveres i directoreit fra forrige trin?
7. Eksekver kommandoen `cd ../..`.
8. Hvor mange _directories_ viser outputtet fra `pwd` kommandoen nu?
9. Eksekver kommandoen `cd ~` (Karakteren hedder tilde. I Ubuntu kan den nogen gange findes med knappen F6).
10. Kommandoen `~` er en "genvej" i Linux, hvad er det en genvej til?
11. I filsystemets rod (`/`), eksekver kommandoen `ls`.
12. I brugerens _home directory_, eksekver kommandoen `touch helloworld.txt`.
13. I brugerens _home directory_, eksekver kommandoen `nano helloworld.txt`.
14. List alle filer og mapper i brugerens _home directory_.
15. List alle filer og mapper i brugerens _home directory_ med flaget `-a`.
16. List alle filer og mapper i brugerens _home directory_ med flaget `-l`.
17. List alle filer og mapper i brugerens _home directory_ med flaget `-la`.
18. I brugerens _home directory_, eksekver kommandoen `mkdir helloWorld`.
19. Eksekver kommandoen `ls -d */`.
20. Eksekver kommandoenn `ls -f`.

## Test dig selv  
_Udfør den følgende opgave med så lidt hjælp som muligt._  

Opret directoryet _minefiler_ i _Home directory_. I _minefiler_ skal der oprettes en fil ved navn `fil1` og en skjult fil ved navn `.skjultfil1`. Udfør herefter følgende:

1. Udskriv en liste til konsollen, der viser alle ikke-skjulte filer.
2. Udskriv en liste til konsollen, der viser alle filer (inklusiv skjulte filer).
3. Gå tilbage til _Home directory_.
4. Lav et skjult directory ved navn `.skjult` (hvordan symboliserede vi tidligere skjulte filer?).


## Links
[Basic Linux commands](https://www.hostinger.com/tutorials/linux-commands)  
[Basic linux commands videos](https://www.youtube.com/watch?v=gd7BXuUQ91w)
