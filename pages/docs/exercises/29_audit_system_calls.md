# Opgave 29 - Overvågning af OS API'et for specifikke systemkald

## Information
Man kan overvåge al kommunikation mellem kørende processer (applikationer) og operativsystemet. Dette gøres ved at overvåge `systemkald`.

## Instruktioner

### Overvågning af processer, der bliver afsluttet
1. Tilføj reglen `auditctl -a always,exit -F arch=b64 -S kill -F key=kill_rule`.
2. Start en baggrundsproces med kommandoen `sleep 600 &`.
3. Find proces-ID'et med `ps aux` for sleep-processen.
![Sleep-proces](./Images/ProcesToKill.jpg)
4. Dræb processen med kommandoen `kill <proces ID>`.
5. Eksekver kommandoen `aureport -i -k | grep kill_rule` og bekræft, at der er lavet nye rækker.  
_Der vil sandsynligvis være flere nye rækker; auditd har ofte sit eget liv._

## Links

