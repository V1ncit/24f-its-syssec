# Opgave 8 - Processer og services i Linux

## Information
Som nævnt i dagens forberedelse er en af egenskaberne ved et operativsystem, at der kan eksekveres mange applikationer på samme tid. I Linux refererer man til kørende applikationer som _processer_. Dette kigger vi på i de følgende opgaver.  

**Fortsæt med at notere i dit Linux cheat sheet**

## Links til beskrivelse af kommandoerne i opgaven:  
[beskrivelse af ps kommandoen](https://man7.org/linux/man-pages/man1/ps.1.html)
  
## Instruktioner
I det første trin skal du se, hvilke processer den nuværende bruger har kørende.  
1. Eksekver kommandoen `ps`. _Resultatet skulle gerne ligne nedstående billede:_  
![PS kommandoen](./Images/PSCommand.jpg)  
I venstre side af outputtet ses Process Id (PID). Hver process i Linux har et unikt ID.
Herefter kommer _TeleTypewriter_ (TTY), som viser hvilken terminal der eksekverede en kommando.
CPU tid (TIME) viser det samlede CPU-tidsforbrug.
Til sidst viser Command (CMD), hvilken kommando der eksekverede processen.  
2. Eksekver kommandoen `ps -aux`   
![PS AUX kommandoen](./Images/PSAUXCommand.jpg)  _Denne gang vises processerne for alle brugere._  
  
Der er et par nye kolonner i outputtet; de mest interessante er %CPU, %MEM og STAT.
%CPU og %MEM viser hver især det procentvise CPU- og hukommelsesforbrug.
STAT viser processorens tilstand: S=Sleep but interruptible, R=Running, og T=Terminated.  
  
I de næste tre trin installerer vi en service i form af Apache webserver.
Typisk bruger man ordet "service", når en applikation tilbyder en "service" til andre. I dette tilfælde en API via netværk (også i slang kaldet en "Web API"), som andre systemer kan benytte sig af (f.eks. en browser).
En service er blot en applikation, som eksekveres på operativsystemet, og den har derfor også en proces.

3. Eksekver kommandoen `sudo apt install apache2 && sudo systemctl unmask apache2`.
4. Udskriv alle processer til konsollen og `grep` på ordet "apache2".  
  
Bemærk tilstanden (STAT) er S. Altså, processen kører ikke, men venter på en hændelse. I de næste trin prøver vi at vække den.

5. Eksekver kommandoen `sudo apt install curl`. _Curl er et værktøj, som kan lave HTTP-forespørgsler til f.eks. Apache2 webserveren._
6. Eksekver kommandoen `curl 127.0.0.1`. _Nu sendes der et HTTP-request til Apache2 webserveren._
7. Gennemse outputtet og bekræft, at det er HTML fra Apache-serveren.
  
Det betyder, at selv om en proces ikke kører (S), kan den ved behov vækkes af operativsystemet i tilfælde af en specifik hændelse.
  
I de sidste trin fokuseres der på at genstarte en proces og at "slå den ihjel".

8. Eksekver kommandoen `nano &`. _Nano er en teksteditor, som vi med ampersand-symbolet (&) kører i baggrunden._
9. Udskriv alle processer til konsollen og grep på ordet 'nano', men undgå at inkludere resultatet fra `grep`-kommandoen selv.   
_Der kommer formodentlig to resultater; den ene er grep-kommandoen, som lige er blevet eksekveret._
10. I kolonne 2 kan du se process ID (PID); eksekver følgende kommando med process-ID'et `kill -1 <Skriv PID her>` (Dette sender SIGHUP-signal og beder processen om at genindlæse sin konfiguration).
11. Udskriv alle processer til konsollen og grep på ordet 'nano'. Outputtet skulle gerne ligne outputtet fra tidligere. Det er fordi processen er genstartet, men ikke slukket; starttid og PID forbliver det samme.
12. Eksekver nu følgende kommando `kill -9 <Skriv PID her>` (Dette sender SIGKILL-signal og tvinger processen til at stoppe med det samme).
13. Nu skulle processen være slukket og ikke synlig, hvis du udskriver processerne.


     
## Links
[Basic Linux commands](https://www.hostinger.com/tutorials/linux-commands)  
[Basic Linux commands videos](https://www.youtube.com/watch?v=gd7BXuUQ91w)
