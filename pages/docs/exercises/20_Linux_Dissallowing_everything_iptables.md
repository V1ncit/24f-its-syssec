# Opgave 20 - Bloker alt indgående trafik
  
## Information
En firewall giver mulighed for at filtrere på mange måder. Som udgangspunkt giver den mulighed for at åbne
op for kommunikation på specifikke netværksporte eller lukke ned for kommunikation på specifikke netværksporte. Som udgangspunkt bør man lukke for kommunikation på alle porte og derefter åbne for de specifikke porte, der er behov for. Altså tilgangen er "Allow list", som udgangspunkt er intet tilladt.

Firewall-regler behandles ofte én ad gangen, i rækkefølge. Dette kaldes en regelkæde.
Reglerne i en regelkæde håndhæves ofte i kronologisk rækkefølge. For eksempel:

Regel 1: Tillad TCP-forbindelser på port 80.  
Regel 2: Forbyd alle UDP-forbindelser.  
Regel 3: Forbyd alle TCP-forbindelser.  
Regel 4: Tillad UDP-forbindelser på port 53.  
  
I overstående tilfælde kommer regel 1 før regel 3. Og derfor tillader firewallen, at der oprettes TCP-forbindelser på port 80. Omvendt kommer regel 2 før regel 4, og derfor er UDP-forbindelser på port 53 ikke tilladt. Når man bruger `-A`-muligheden til at indsætte en regel, bliver den indsat til sidst i regelkæden. Når firewall-reglerne eksekveres kronologisk, bør en "drop alt trafik"-regel altid komme til sidst i regelkæden.

## Instruktioner
1. Udskriv regelkæden og notér dig, hvordan den ser ud. (Regelkæden er tidligere blevet udskrevet i [opgave 19](./19_Linux_Installing_iptables.md), trin 4)
2. Lav en regel i slutningen af regelkæden, som dropper alt trafik, med kommandoen `sudo iptables -A INPUT -j DROP`.
3. Udskriv regelkæden igen og notér forskellen fra trin 1.

### Bonusopgave
Du kan prøve at anvende en netværksscanner såsom nmap til at skanne dine netværksporte før og efter du opsætter firewall-regler.

## Links