# Uge 15 - Eftermiddags opgave - Vælg en dektektering fra Wazuh blogs

## Information

Generelt bliver Wazuh dokumentationen kun opdateret, når Wazuh softwaren bliver opdateret. Det betyder
at Wazuh dokumentationen ikke bliver opdateret når der F.eks. bliver opdaget nye større sårbarheder.
I stedet har teamet bag Wazuh en blog, som kan ses [her](https://wazuh.com/blog/).
Bloggen udkommer med nyheder om Wazuh, releases og guides til opsætninger til detektering.
F.eks. er der en guide til detektering af sårbarheden [XZ her](https://wazuh.com/blog/detecting-xz-utils-vulnerability-cve-2024-3094-with-wazuh/).
  
Gruppen skal finde en detektering under afsnittet `Engineering` og implementer denne.
  
Opgaven er tænkt som en hjælp til semester projektet. Og formålet med opgaven er at gøre
gruppen bekendt med yderlige Wazuh resourcer, og brugen af disse.


## Instruktioner
1. Udvælg en detektering fra afsnittet [Engineering](https://wazuh.com/blog/category/engineering/)
2. Implementer detekteringen.


## Links
[Wazuh blog](https://wazuh.com/blog/)
