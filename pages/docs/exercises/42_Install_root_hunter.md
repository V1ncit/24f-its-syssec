# Opgave 42 - Installer rootkit hunter

## Information
I denne opgave skal softwaren rootkit hunter(Rkhunter) installeres.
Rkhunter er god til at finde den  specialiseret malware af typen rootkit.

Rootkit  generelt  kan være svære at finde end andet malware,  da de er udviklet
til at holde sig skjult, og skjule andet malware.


## Instruktioner
1. Installer softwaren med kommandoen `sudo apt install rkhunter`
2. I menuen der dukker op, vælg `2 no configuration`
3. åben konfigurations filen `/etc/rkhunter.conf`
4. Ændre værdien af `MIRRORS_MODE` til 0.
5. Ændre værdien af `UPDATE_MIRRORS` til 1.
6. Ændre værdien af  `WEB_CMD` til "".  
![Rkhunter config](./Images/malware/rkhunter_config.jpg)  
7. Opdatere rootkit definationerne med `rkhunter --update`


## Links
[Rootkit hunter docs](https://rkhunter.sourceforge.net/)