# Opgave 44 - statisk analyse af tekst strenge

## Information
I denne opgave skal der udføres en statisk analyse baseret på tekst strenge i en binær file.
Denne fremgangsmåde er anvendelig  ved F.eks. endnu ukendt malware.  F.eks ekstrakter man tekst
stregene `www.malicious-url.com` eller en kendt ondsindet ip som softwaren bruger, kan dette være
en indikator på at softwaren er malware.


## Instruktioner
1. `sudo apt install binutils`
2. Opret et directory i `/`
3. Download test file med kommandoen `sudo wget https://secure.eicar.org/eicarcom2.zip` i directoriet fra forrige skridt
4. ekstrakter tekst strenge med kommandoen `strings eicarcom2.zip`
5. Reflekter over hvorfor tekststrenge  kan indiker  malware


## Links
[strings tool](https://linux.die.net/man/1/strings )