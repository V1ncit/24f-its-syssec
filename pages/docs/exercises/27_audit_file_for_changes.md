# Opgave 27 - Audit af en fil for ændringer

## Information
For at definere begivenheder, der udløser en "audit log", skal man opsætte en auditregel enten ved at tilføje reglen via værktøjet `auditctl` eller ved at tilføje en regel i auditregelfilen `/etc/audit/audit.rules`. 


## Instruktioner

### Tilføj auditregel med auditctl
1. Opret en auditregel for hver gang der skrives eller ændres på filen `/etc/passwd` med kommandoen `auditctl -w /etc/passwd -p wa -k user_change`.  

_Muligheden `-w` står for "where" og refererer til den fil, hvor auditreglen skal gælde. `-p` står for "permissions" og angiver hvilke rettigheder, der skal overvåges. I eksemplet står der `wa`, hvilket står for "attribute" og "write". Hvis filens metadata ændres, eller der skrives til filen, udløser det en begivenhed og der bliver oprettet en auditlog. `-k` står for "key", og tildeler begivenheden et navn man kan søge på_

2. Udskriv en rapport over alle hændelser, der er logget med kommandoen `aureport -i -k | grep user_change`.  

_Muligheden `-i` står for "interpret", og betyder at numeriske enheder såsom bruger-ID bliver fortolket som brugernavn. `-k` står for "key", hvilket betyder at reglen, der udløste auditloggen, skal vises._  

Formattet rapporten bliver udskrevet i, er _Datetime Key success Executing event (process, bruger samt process id)_ .
Da flere forskellige processer interagerer med `passwd` filen, tilføjes der også flere log linjer, som kan ses i rapporten.

1. Tilføj noget tekst i bunden af filen `/etc/passwd`.
2. Udskriv loggen igen, med `aureport -i -k | grep user_change`, og bekræft at der nu er kommet to ny række i rapporten. _en linje for skrivning til filen, og en for ændring af filens metadata_
3. Brug kommandoen `ausearch -i -k user_change`. Her skulle du gerne kunne finde en log, som ligner den nedenstående.  
   ![Ausearch](./Images/ausearchforfileChange.jpg)  

Generelt kan det være en smule rodet at skulle finde rundt i disse logs. Oftest bliver der produceret mere end én log, når en begivenhed udløser en regel. Forskellen på `ausearch` og `aureport` er, at `ausearch` viser flere oplysninger, men kan dermed også blive mere uoverskuelig.

### Tilføj auditregel med auditregelkonfigurationsfilen
Ulempen ved at tilføje regler direkte med auditctl er, at regler ikke bliver persistente. Det vil sige, de bliver ikke gemt og forsvinder, når auditd eller hele systemet genstartes. For at gemme reglerne skal man lave en regelfil og gemme den i `/etc/audit/rules.d/`.

1. Åben filen `/etc/audit/audit.rules`. Den skulle gerne se ud som vist nedenfor.  
![Audit regelfil](./Images/AuditRuleFile.jpg)  

_Regelfilen bliver indlæst ved opstart af auditd, og alle regler heri bliver de gældende auditregler._

Dette er auditd's primære konfigurationsfil, som indeholder alle regelkonfigurationerne. Men bemærk kommentaren øverst i filen. Den fortæller, at denne fil bliver autogenereret ud fra indholdet af `/etc/audit/audit.d` directory (ikke en særlig intuitiv måde at lave konfigurationsopsætning på).

2. Opret en ny fil, som indeholder reglen, med kommandoen `sh -c "auditctl -l > /etc/audit/rules.d/custom.rules"`.  
_**For at kommandoen virker, skal reglen der blev lavet i trin 1 i forrige afsnit stadig være gældende. Kontroller eventuelt med `auditctl -l` inden trin 2 udføres._  

3. Genstart auditd med kommandoen `systemctl restart auditd`.
4. Udskriv indholdet af `/etc/audit/audit.rules`. Resultatet bør ligne det, som vises på billedet nedenfor.  
![Audit konfigurationsfil med regel](./Images/AuditConfigFileWithRule.jpg)  

**Slet filen /etc/audit/rules.d/custom.rules efter øvelsen.**

### Audit alle filændringer
Man kan også udskrive alle ændringer, der er blevet lavet på overvågede filer.

1. Eksekver kommandoen `aureport -f`. Med denne bør du få en udskrift, der ligner nedenstående.  
![Alle filer audit](./Images/AllFileAudits.jpg)  

Den første række med nummeret 1 blev indsat den 27. marts 2023 kl. 20:11. Begivenheden, som udløste loggen, var en udskrift af filen `tmp.txt`. Udskrivningen blev lavet af brugeren med ID 1000. Og udskrivningen blev eksekveret med kommandoen `cat`.  

2. Ændre kommandoen fra trin 1, så den skriver brugernavn i stedet for bruger-ID. (Hvordan skal bruger id'et fortolkes?)

## Links
[Audit logfilsystem](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/security_guide/sec-understanding_audit_log_files)  
[Openat-systemkald](https://manpages.ubuntu.com/manpages/xenial/man2/open.2freebsd.html)  
[Aureport-manualside](https://man7.org/linux/man-pages/man8/aureport.8.html)