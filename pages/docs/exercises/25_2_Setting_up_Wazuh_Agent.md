# Opgave 25.2 Eftermiddag - Opsætning af Wazuh agent

## Information
I forrige uge opsatte i en Wazuh-server. Denne uge skal I opsætte en Wazuh-agent på den Ubuntu-instans, der **ikke** fungerer som vært for Wazuh-serveren, altså den Ubuntu-VM på Proxmox med navnet _Target host_.

Wazuh overvåger som udgangspunkt et system ved at analysere linjer fra logfiler på det enkelte system. For at Wazuh kan modtage loglinjer fra et system, skal der være en applikation på det overvågede system, som sender disse loglinjer til Wazuh. Denne type applikation kaldes i Wazuh-domænet en agent.

Helt grundlæggende skaber log filer i sig selv ikke værdi eller øger sikkerheden, hvis der ikke er nogen som
læser eller analyser dem. SIEM systemer såsom Wazuh kan automatisere log analyse for os, og sender alarmer ved
specifikke mønstre eller begivenheder i et en log file.

I denne opgave skal i lave en opsætning af en Wazuh agent på Ubuntu instansen _Target host_ og valider at
agenten forbinder til den Wazuh server i satte op i forrige uge. Herefter skal i prøve at udløse en alarm ved at tilføje en ny bruger på den overvåget Ubuntu instanse.

Formålet med øvelsen er at der skabes en grundlægende forståelse for hvordan Wazuh agenter fungerer, samt hvordan
et SIEM system anvender log filer.

## Instruktioner
  
### Opsætning og afprøving af Wazuh agent.
1. Lav opsætningen af Wazuh agenten på _Target host_ ved følge [dokumenationen](https://documentation.wazuh.com/current/installation-guide/wazuh-agent/wazuh-agent-package-linux.html)
_Den sidste del med 'Disable Wazuh update' behøver i ikke at følge_
2. Verificer at Wazuh agent har forbindeles til Wazuh server ved at følge guiden i [afsnittet Using the Wazuh dashboard](https://documentation.wazuh.com/current/user-manual/agents/agent-connection.html#checking-connection-with-the-wazuh-manager)
3. På den overvåget Ubuntu instansen _Target host_, tilføj en ny bruger med navnet `darth`.
4. Log ind på Wazuh dashboard, og klik ind på _Security events_
5. I _Security events_ under _Security alerts_, Valider at Wazuh har detekteret en begivenhed der falder under Mitre teknikken T1136, i forbindelse med at den nye bruger blev oprettet
6. Udvid informationen på alarmen der blev udløst i forbindelse med oprettelsen af den nye bruger ved at klikke på pilen til venstre for alarm linjen, og se om i kan identificere hvilken log file wazuh agenten læste fra(Feltet med information hedder location)
7. Undersøg hvor meget Information i kan finde om den nye bruger i alarmen.

## Links
[Setting up a wazuh agent](https://documentation.wazuh.com/current/installation-guide/wazuh-agent/wazuh-agent-package-linux.html)