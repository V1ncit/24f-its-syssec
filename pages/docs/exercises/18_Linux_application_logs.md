# Opgave 18 - Applikationlogs

## Information
I denne opgave skal der arbejdes med applikationlogs. Applikationlogs er de logfiler der anvendes af en applikation
som eksekveres på hosten. Applikationen som der skal læse log filer fra, er web serveren Apache2, og log filen der
skal læses er adgangsloggen som blandt andet viser hvilken ip adresser der har tilgået serveren.
Dagens opsætning er en relativ simple opsætning. Formålet er blot at vise hvordan appliaktions logs fungerer, samt at
applikationlogs kan opbevares som tekst filer på hosten.
  
I opgaven skal du først følge en guide som viser opsætningen af en Apache2 web server. Herefter skal du 
valider at serveren virker, ved at tilgå server fra anden host. Til sidst skal du verificerer at du kan se
en log linje i apache2 applikationloggen for adgang.

Du kan få et fuldt overblik over apache2 logning [her](https://www.loggly.com/ultimate-guide/apache-logging-basics/)
Og du kan se lokationen af Apache2 log filer på Ubuntu, i afsnittet [Application logs, her.](https://help.ubuntu.com/community/LinuxLogFiles)

 
## Instruktioner
1. Følg guiden for opsætning af Apache2 web server her [her](https://ubuntu.com/tutorials/install-and-configure-apache#1-overview)
2. Udskriv indholdet af apache's adgangs log som findes i  `/var/log/apache2/access.log`.
3. Tilgå hjemmesiden fra en maskine der ikke er host maskinen.(Guiden viser eksempel. Default port for Apache server er port 80)
4. Verificerer at den sidste log  i filen viser at den sidste maskine som har tilgået web serveren er den som du brugte til at udføre _trin 3_
5. Eksekver kommandoen `tail -f access.log`
6. Udfør _trin 3_ et par gange igen, og verificer at der kommer en ny log entry hver gang.

Mange af de tidligere opgaver har været centeret om log filer fra operativ systemet. Men det er meget
vigtig at huske, at applikationer ofte har sine egene log filer (og autentificering). Og at hver applikation eksekveres med et sæt bruger rettigheder.

## Links
  
[Apache logging basics](https://www.loggly.com/ultimate-guide/apache-logging-basics/)
[Linux log files](https://help.ubuntu.com/community/LinuxLogFiles)
