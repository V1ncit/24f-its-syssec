# Opgave 32 - Opsætning af agent

## Information
Wazuh interagerer med hosts via "Agenter". En agent er en applikation som eksekver på den overvåget host.
En agent kan indsamle data til overvågning. Men også eksekver kommandoer på hosten, som reaktion på hændelser.
  
Deployering af Wazuh agenter er som oftest eksekvering af en enkelt kommandoen som man kan få generet i Wazuh dashboard.

**Denne og de følgende opgaver tager udgangspunkt i at følgende VM'er er tilgængelige:**  
- Wazuh Server (Se opgave 31)  
- En Ubuntu 22.04 server (En anden nye Debian distro burde også virke fint). Denne vil bliver referet til som _Den overvåget host_ fremadrettet.  
- En Kali Linux VM, denne vil fremadrettede blive referet til som _Angribende host_  


## Instruktioner
 
1. Vælg _agents_ i Wazuh dashboard menuen.  
![Wazuh agents menu](./Images/Wazuh_agent/Wazuh_server_agent%20menu.jpg)  
**Følg de viste trin for udfyldelse af Wazuh agent template oprettelse**  
_Trin numrene følger Wazuh template trin numrene_   
   1. Vælg Ubuntu som OS  
   2. Vælg ubuntu 15+  som OS  
   3. Vælg x86_64 som arkitektur  
   4. Indtast ip adressen til Wazuh server VM'en  
   5. Tast _Target-host-1_ som navnet på agenten, og vælg gruppen _default_  
   6. Kopier kommandoen fra _Install and enroll_ og Paste kommandoen ind i det VM OS hvor agenten skal overvåge  
   7. Eksekver kommandoerne vist i _Start the agent_  
   8. Eksekver kommandoen `systemctl status wazuh-agent` og bekræft at agenten er aktiv  
   9. Bekræft i _Wazuh dashboard_ at agent er forbundet og er aktiv  
   ![Active Wazuh agent](./Images/Wazuh_agent/Active_agent.jpg)

## Links
[Deploy agent with template from wazuh dashboard](https://documentation.wazuh.com/current/installation-guide/wazuh-agent/index.html)  
[How Wazuh agents works](https://documentation.wazuh.com/current/getting-started/components/wazuh-agent.html)  
[Deploying Wazuh agent without dashboard template](https://documentation.wazuh.com/current/installation-guide/wazuh-agent/wazuh-agent-package-linux.html)  
[Wazuh agent troubleshooting](https://documentation.wazuh.com/current/user-manual/agent-enrollment/troubleshooting.html)
